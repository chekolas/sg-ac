@component('emails::message')
# Introduction

The body of your message.

@component('emails::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
