<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Content;
use Faker\Generator as Faker;

$factory->define(Content::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
        'description' => $faker->paragraphs(4, true),
        'url' => 'https://www.youtube.com/embed/r_nhE4yuuKo',
        'duration' => 7700
    ];
});
