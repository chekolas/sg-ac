<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Answer;
use Faker\Generator as Faker;

$factory->define(Answer::class, function (Faker $faker) {
    return [
        'description' => $faker->paragraph,
        'status' => $faker->randomElement([0, 1])
    ];
});
