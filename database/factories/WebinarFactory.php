<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Webinar;
use Faker\Generator as Faker;

$factory->define(Webinar::class, function (Faker $faker) {
    $startDateTime = \Illuminate\Support\Carbon::parse($faker->dateTimeBetween('now', '+1 months'));
    $endDateTime = \Illuminate\Support\Carbon::parse($startDateTime)->addHours($faker->numberBetween(1, 3));
    return [
        'name' => $faker->sentence,
        'description' => $faker->paragraphs(6, true),
        'started_at' => $startDateTime,
        'ended_at' => $endDateTime,
        'venue' => $faker->randomElement(['youtube', 'zoom', 'sga']),
        'url' => $faker->url,
        'code' => $faker->word,
        'capacity' => $faker->numberBetween(100, 1500),
        'status' => $faker->numberBetween(0, 1)
    ];
});
