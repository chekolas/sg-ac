<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Track;
use Faker\Generator as Faker;

$factory->define(Track::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
        'description' => $faker->paragraphs(4, true),
        'status' => 1
    ];
});
