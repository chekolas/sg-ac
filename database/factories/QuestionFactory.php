<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Question;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {
    return [
        'description' => $faker->paragraphs(2, true),
        'grade' => $faker->randomElement([10, 25, 50, 75, 100]),
    ];
});
