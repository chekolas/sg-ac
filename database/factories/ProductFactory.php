<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Storage;

$factory->define(Product::class, function (Faker $faker) {
    $normal = $faker->numberBetween(100000, 300000);
    $discount = $faker->numberBetween(80000, $normal);
    $special = $faker->numberBetween(30000, $discount);
    return [
        'avatar' => Storage::url('temp/' . $faker->image(
            storage_path('app/public/temp'),
            512,
            512,
                false
        )),
        'name' => $faker->sentence,
        'abstract' => $faker->paragraph(2, true),
        'description' => $faker->paragraphs(4, true),
        'normal' => $normal,
        'discount' => $discount,
        'special' => $special,
        'status' => $faker->numberBetween(0,2)
    ];
});
