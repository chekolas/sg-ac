<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = ['administrator','user'];

        foreach ($roles as $role) {
            $r = new \Spatie\Permission\Models\Role();
            $r->name = $role;
            $r->guard_name = 'api';
            $r->save();
        }
    }
}
