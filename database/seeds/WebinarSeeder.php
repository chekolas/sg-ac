<?php

use Illuminate\Database\Seeder;

class WebinarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Webinar::class, 50)
            ->create()
            ->each(function ($webinar) {
                $webinar->product()->associate(factory(App\Product::class)->create());
                $webinar->save();
            });
    }
}
