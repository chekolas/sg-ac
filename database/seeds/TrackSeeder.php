<?php

use Illuminate\Database\Seeder;

class TrackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Track::class, 30)
            ->create()
            ->each(function ($track) {
                $track->product()->associate(factory(App\Product::class)->create());

                $faker = Faker\Factory::create();
                $generateContents = $faker->numberBetween(4, 10);
                factory(App\Content::class, $generateContents)
                    ->create()
                    ->each(function ($content, $index) use ($track) {
                       $content->tracks()->save($track, ['page' => $index + 1]);
                    });

                $generateQuestions = $faker->numberBetween(6, 20);
                factory(App\Question::class, $generateQuestions)
                    ->create()
                    ->each(function ($question) use ($track) {
                        $question->tracks()->save($track);
                        factory(App\Answer::class, 4)
                            ->create()
                            ->each(function ($answer) use ($question) {
                                $answer->question()->associate($question);
                            });
                    });
            });
    }
}
