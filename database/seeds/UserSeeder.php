<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // seeding one administrator
        echo 'Checking for administrator'.PHP_EOL;

        $admin = \Spatie\Permission\Models\Role::where('name', 'administrator')->first();

        if (!is_null($admin)) {
            echo 'Administrator Role found' . PHP_EOL;

            $user = new \App\User();
            $user->name = 'Admin';
            $user->email = 'app@salamganehsa.id';
            $user->phone = '08123456789';
            $user->password = \Illuminate\Support\Facades\Hash::make('SG20222@c4d3MY');
            $user->code = '814131';
            $user->email_verified_at = now();
            $user->status = 1;
            $user->save();

            $user->assignRole($admin);

            echo 'First user is an administrator now' . PHP_EOL;
        } else {
            echo 'Please run Role seeder first'.PHP_EOL;
        }
    }
}
