<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadTrackPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_track', function (Blueprint $table) {
            $table->unsignedBigInteger('upload_id')->index();
            $table->unsignedBigInteger('track_id')->index();
        });
        Schema::table('upload_track', function (Blueprint $table) {
            $table->foreign('upload_id')
                ->references('id')
                ->on('uploads');
            $table->foreign('track_id')
                ->references('id')
                ->on('tracks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('upload_track', function (Blueprint $table) {
            $table->dropIndex(['upload_id']);
            $table->dropIndex(['track_id']);
            $table->dropForeign(['upload_id']);
            $table->dropForeign(['track_id']);
        });
        Schema::dropIfExists('upload_track');
    }
}
