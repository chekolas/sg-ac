<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLecturerUploadPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturer_upload', function (Blueprint $table) {
            $table->unsignedBigInteger('lecturer_id')->index();
            $table->unsignedBigInteger('upload_id')->index();
        });
        Schema::table('lecturer_upload', function (Blueprint $table) {
            $table->foreign('lecturer_id')
                ->on('lecturers')
                ->references('id');
            $table->foreign('upload_id')
                ->on('uploads')
                ->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lecturer_upload', function (Blueprint $table) {
            $table->dropIndex(['lecturer_id']);
            $table->dropIndex(['upload_id']);
            $table->dropForeign(['lecturer_id']);
            $table->dropForeign(['upload_id']);
        });
        Schema::dropIfExists('lecturer_upload');
    }
}
