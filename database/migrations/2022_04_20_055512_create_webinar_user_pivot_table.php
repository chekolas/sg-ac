<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebinarUserPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_webinar', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('webinar_id')->index();
            $table->tinyInteger('attendance')->default(0);
            $table->timestamp('join_at')->nullable();
            $table->timestamp('leave_at')->nullable();
        });

        Schema::table('user_webinar', function (Blueprint $table) {
            $table->foreign('webinar_id')
                ->on('webinars')
                ->references('id');
            $table->foreign('user_id')
                ->on('users')
                ->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_webinar', function (Blueprint $table) {
            $table->dropIndex(['user_id']);
            $table->dropIndex(['webinar_id']);
            $table->dropForeign(['user_id']);
            $table->dropForeign(['webinar_id']);
        });
        Schema::dropIfExists('user_webinar');
    }
}
