<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductVoucherPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_voucher', function (Blueprint $table) {
            $table->unsignedBigInteger('product_id')->index();
            $table->unsignedBigInteger('voucher_id')->index();
        });

        Schema::table('product_voucher', function (Blueprint $table) {
            $table->foreign('product_id')
                ->references('id')
                ->on('products');
            $table->foreign('voucher_id')
                ->references('id')
                ->on('vouchers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_voucher', function (Blueprint $table) {
            $table->dropIndex(['product_id']);
            $table->dropIndex(['voucher_id']);
            $table->dropForeign(['product_id']);
            $table->dropForeign(['voucher_id']);
        });
        Schema::dropIfExists('product_voucher');
    }
}
