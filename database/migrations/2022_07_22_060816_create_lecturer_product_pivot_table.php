<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLecturerProductPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturer_product', function (Blueprint $table) {
            $table->unsignedBigInteger('lecturer_id')->index();
            $table->unsignedBigInteger('product_id')->index();
        });

        Schema::table('lecturer_product', function (Blueprint $table) {
            $table->foreign('lecturer_id')
                ->on('lecturers')
                ->references('id');

            $table->foreign('product_id')
                ->references('id')
                ->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lecturer_product', function (Blueprint $table) {
            $table->dropIndex(['lecturer_id']);
            $table->dropIndex(['product_id']);
            $table->dropForeign(['lecturer_id']);
            $table->dropForeign(['product_id']);
        });
        Schema::dropIfExists('lecturer_product');
    }
}
