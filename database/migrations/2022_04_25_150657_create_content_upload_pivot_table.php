<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentUploadPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_upload', function (Blueprint $table) {
            $table->unsignedBigInteger('content_id')->index();
            $table->unsignedBigInteger('upload_id')->index();
        });
        Schema::table('content_upload', function (Blueprint $table) {
            $table->foreign('content_id')
                ->references('id')
                ->on('contents');
            $table->foreign('upload_id')
                ->references('id')
                ->on('uploads');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('content_upload', function (Blueprint $table) {
            $table->dropIndex(['content_id']);
            $table->dropIndex(['upload_id']);
            $table->dropForeign(['content_id']);
            $table->dropForeign(['upload_id']);
        });
        Schema::dropIfExists('content_upload');
    }
}
