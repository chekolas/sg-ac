<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionUploadPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_upload', function (Blueprint $table) {
            $table->unsignedBigInteger('question_id')->index();
            $table->unsignedBigInteger('upload_id')->index();
        });
        Schema::table('question_upload', function (Blueprint $table) {
            $table->foreign('question_id')
                ->on('questions')
                ->references('id');
            $table->foreign('upload_id')
                ->on('uploads')
                ->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('question_upload', function (Blueprint $table) {
            $table->dropIndex(['question_id']);
            $table->dropIndex(['upload_id']);
            $table->dropForeign(['question_id']);
            $table->dropForeign(['upload_id']);
        });
        Schema::dropIfExists('question_upload');
    }
}
