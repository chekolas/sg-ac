<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentLecturerPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_lecturer', function (Blueprint $table) {
            $table->unsignedBigInteger('content_id')->index();
            $table->unsignedBigInteger('lecturer_id')->index();
        });
        Schema::table('content_lecturer', function (Blueprint $table) {
            $table->foreign('content_id')
                ->references('id')
                ->on('contents');
            $table->foreign('lecturer_id')
                ->references('id')
                ->on('lecturers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('content_lecturer', function (Blueprint $table) {
            $table->dropIndex(['content_id']);
            $table->dropIndex(['lecturer_id']);
            $table->dropForeign(['content_id']);
            $table->dropForeign(['lecturer_id']);
        });
        Schema::dropIfExists('content_lecturer');
    }
}
