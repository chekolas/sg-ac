<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageUploadPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_upload', function (Blueprint $table) {
            $table->unsignedBigInteger('page_id')->index();
            $table->unsignedBigInteger('upload_id')->index();
        });
        Schema::table('page_upload', function (Blueprint $table) {
            $table->foreign('page_id')
                ->references('id')
                ->on('pages');
            $table->foreign('upload_id')
                ->references('id')
                ->on('uploads');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('page_upload', function (Blueprint $table) {
            $table->dropIndex(['page_id']);
            $table->dropIndex(['upload_id']);
            $table->dropForeign(['page_id']);
            $table->dropForeign(['upload_id']);
        });
        Schema::dropIfExists('page_upload');
    }
}
