<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoucherUserPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher_user', function (Blueprint $table) {
            $table->unsignedBigInteger('voucher_id')->index();
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedTinyInteger('is_used')->default(0)->index();
        });

        Schema::table('voucher_user', function (Blueprint $table) {
            $table->foreign('voucher_id')
                ->references('id')
                ->on('vouchers');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voucher_user', function (Blueprint $table) {
            $table->dropIndex(['voucher_id']);
            $table->dropIndex(['user_id']);
            $table->dropForeign(['voucher_id']);
            $table->dropForeign(['user_id']);
        });
        Schema::dropIfExists('voucher_user');
    }
}
