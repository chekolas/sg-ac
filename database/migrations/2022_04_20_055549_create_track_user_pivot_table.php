<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrackUserPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('track_user', function (Blueprint $table) {
            $table->unsignedBigInteger('track_id')->index();
            $table->unsignedBigInteger('user_id')->index();
            $table->timestamp('started_at')->nullable();
            $table->timestamp('ended_at')->nullable();
        });

        Schema::table('track_user', function (Blueprint $table) {
            $table->foreign('track_id')
                ->on('tracks')
                ->references('id');
            $table->foreign('user_id')
                ->on('users')
                ->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('track_user', function (Blueprint $table) {
            $table->dropIndex(['track_id']);
            $table->dropIndex(['user_id']);
            $table->dropForeign(['track_id']);
            $table->dropForeign(['user_id']);
        });
        Schema::dropIfExists('track_user');
    }
}
