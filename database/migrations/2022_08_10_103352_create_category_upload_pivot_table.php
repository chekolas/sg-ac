<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryUploadPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_upload', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->index();
            $table->unsignedBigInteger('upload_id')->index();
        });

        Schema::table('category_upload', function (Blueprint $table) {
            $table->foreign('category_id')
                ->on('categories')
                ->references('id');
            $table->foreign('upload_id')
                ->on('uploads')
                ->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_upload', function (Blueprint $table) {
            $table->dropIndex(['category_id']);
            $table->dropIndex(['upload_id']);
            $table->dropForeign(['category_id']);
            $table->dropForeign(['upload_id']);
        });
        Schema::dropIfExists('category_upload');
    }
}
