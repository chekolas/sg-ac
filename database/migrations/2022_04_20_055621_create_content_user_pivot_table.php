<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentUserPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_user', function (Blueprint $table) {
            $table->unsignedBigInteger('content_id')->index();
            $table->unsignedBigInteger('user_id')->index();
            $table->timestamp('started_at')->nullable();
            $table->timestamp('ended_at')->nullable();
        });

        Schema::table('content_user', function (Blueprint $table) {
            $table->foreign('content_id')
                ->references('id')
                ->on('contents');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('content_user', function (Blueprint $table) {
            $table->dropIndex(['content_id']);
            $table->dropIndex(['user_id']);
            $table->dropForeign(['content_id']);
            $table->dropForeign(['user_id']);
        });

        Schema::dropIfExists('content_user');
    }
}
