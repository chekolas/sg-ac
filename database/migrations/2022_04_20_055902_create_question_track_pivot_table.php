<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionTrackPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_track', function (Blueprint $table) {
            $table->unsignedBigInteger('question_id')->index();
            $table->unsignedBigInteger('track_id')->index();
        });

        Schema::table('question_track', function (Blueprint $table) {
            $table->foreign('question_id')
                ->references('id')
                ->on('questions');

            $table->foreign('track_id')
                ->references('id')
                ->on('tracks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('question_track', function (Blueprint $table) {
            $table->dropIndex(['question_id']);
            $table->dropIndex(['track_id']);
            $table->dropForeign(['question_id']);
            $table->dropForeign(['track_id']);
        });
        Schema::dropIfExists('question_track');
    }
}
