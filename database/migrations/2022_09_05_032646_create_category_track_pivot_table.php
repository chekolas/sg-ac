<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryTrackPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_track', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->index();
            $table->unsignedBigInteger('track_id')->index();
        });

        Schema::table('category_track', function (Blueprint $table) {
            $table->foreign('category_id')
                ->on('categories')
                ->references('id');
            $table->foreign('track_id')
                ->on('tracks')
                ->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_track', function (Blueprint $table) {
            $table->dropIndex(['category_id']);
            $table->dropIndex(['track_id']);
            $table->dropForeign(['category_id']);
            $table->dropForeign(['track_id']);
        });
        Schema::dropIfExists('category_track');
    }
}
