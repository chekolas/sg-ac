<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadVoucherPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_voucher', function (Blueprint $table) {
            $table->unsignedBigInteger('upload_id')->index();
            $table->unsignedBigInteger('voucher_id')->index();
        });

        Schema::table('upload_voucher', function (Blueprint $table) {
            $table->foreign('upload_id')
            ->on('uploads')
            ->references('id');

            $table->foreign('voucher_id')
            ->on('vouchers')
            ->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('upload_voucher', function (Blueprint $table) {
            $table->dropIndex(['upload_id']);
            $table->dropIndex(['voucher_id']);
            $table->dropForeign(['upload_id']);
            $table->dropForeign(['voucher_id']);
        });
        Schema::dropIfExists('upload_voucher_pivot');
    }
}
