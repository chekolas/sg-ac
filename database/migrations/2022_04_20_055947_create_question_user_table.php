<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('question_id')->index();
            $table->unsignedBigInteger('answer_id')->index();
            $table->integer('grade')->nullable();
            $table->timestamps();
        });

        Schema::table('question_user', function (Blueprint $table) {
            $table->foreign('question_id')
                ->references('id')
                ->on('questions');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
            $table->foreign('answer_id')
                ->references('id')
                ->on('answers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('question_user', function (Blueprint $table) {
            $table->dropIndex(['user_id']);
            $table->dropIndex(['question_id']);
            $table->dropIndex(['answer_id']);
            $table->dropForeign(['user_id']);
            $table->dropForeign(['question_id']);
            $table->dropForeign(['answer_id']);
        });
        Schema::dropIfExists('question_user');
    }
}
