<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductUploadPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_upload', function (Blueprint $table) {
            $table->unsignedBigInteger('product_id')->index();
            $table->unsignedBigInteger('upload_id')->index();
            $table->integer('slide')->nullable();
        });

        Schema::table('product_upload', function (Blueprint $table) {
            $table->foreign('product_id')
                ->on('products')
                ->references('id');
            $table->foreign('upload_id')
                ->on('uploads')
                ->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_upload', function (Blueprint $table) {
            $table->dropIndex(['product_id']);
            $table->dropIndex(['upload_id']);
            $table->dropForeign(['product_id']);
            $table->dropForeign(['upload_id']);
        });
        Schema::dropIfExists('product_upload');
    }
}
