<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryWebinarPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_webinar', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->index();
            $table->unsignedBigInteger('webinar_id')->index();
        });

        Schema::table('category_webinar', function (Blueprint $table) {
            $table->foreign('category_id')
                ->on('categories')
                ->references('id');
            $table->foreign('webinar_id')
                ->on('webinars')
                ->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_webinar', function (Blueprint $table) {
            $table->dropIndex(['category_id']);
            $table->dropIndex(['webinar_id']);
            $table->dropForeign(['category_id']);
            $table->dropForeign(['webinar_id']);
        });
        Schema::dropIfExists('category_webinar');
    }
}
