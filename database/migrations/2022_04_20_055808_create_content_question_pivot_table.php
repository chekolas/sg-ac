<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentQuestionPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_question', function (Blueprint $table) {
            $table->unsignedBigInteger('content_id')->index();
            $table->unsignedBigInteger('question_id')->index();
            $table->integer('time');
        });

        Schema::table('content_question', function (Blueprint $table) {
            $table->foreign('content_id')
                ->on('contents')
                ->references('id');
            $table->foreign('question_id')
                ->on('questions')
                ->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('content_question', function (Blueprint $table) {
            $table->dropIndex(['content_id']);
            $table->dropIndex(['question_id']);
            $table->dropForeign(['content_id']);
            $table->dropForeign(['question_id']);
        });
        Schema::dropIfExists('content_question');
    }
}
