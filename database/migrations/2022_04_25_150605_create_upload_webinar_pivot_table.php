<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadWebinarPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_webinar', function (Blueprint $table) {
            $table->unsignedBigInteger('upload_id')->index();
            $table->unsignedBigInteger('webinar_id')->index();
        });

        Schema::table('upload_webinar', function (Blueprint $table) {
            $table->foreign('upload_id')
                ->references('id')
                ->on('uploads');
            $table->foreign('webinar_id')
                ->references('id')
                ->on('webinars');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('upload_webinar', function (Blueprint $table) {
            $table->dropIndex(['upload_id']);
            $table->dropIndex(['webinar_id']);
            $table->dropForeign(['upload_id']);
            $table->dropForeign(['webinar_id']);
        });
        Schema::dropIfExists('upload_webinar');
    }
}
