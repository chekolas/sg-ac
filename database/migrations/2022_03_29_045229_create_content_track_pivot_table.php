<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentTrackPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_track', function (Blueprint $table) {
            $table->unsignedBigInteger('content_id')->index();
            $table->unsignedBigInteger('track_id')->index();
            $table->integer('page')->default(1);
        });

        Schema::table('content_track', function (Blueprint $table) {
            $table->foreign('content_id')
                ->references('id')
                ->on('contents');
            $table->foreign('track_id')
                ->references('id')
                ->on('tracks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('content_track', function (Blueprint $table) {
            $table->dropIndex(['content_id']);
            $table->dropIndex(['track_id']);
            $table->dropForeign(['content_id']);
            $table->dropForeign(['track_id']);
        });
        Schema::dropIfExists('content_track');
    }
}
