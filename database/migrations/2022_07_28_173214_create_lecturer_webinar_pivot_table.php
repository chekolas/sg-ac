<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLecturerWebinarPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturer_webinar', function (Blueprint $table) {
            $table->unsignedBigInteger('lecturer_id')->index();
            $table->unsignedBigInteger('webinar_id')->index();
        });

        Schema::table('lecturer_webinar', function (Blueprint $table) {
            $table->foreign('lecturer_id')
                ->references('id')
                ->on('lecturers');
            $table->foreign('webinar_id')
                ->references('id')
                ->on('webinars');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lecturer_webinar', function (Blueprint $table) {
            $table->dropIndex(['lecturer_id']);
            $table->dropIndex(['webinar_id']);
            $table->dropForeign(['lecturer_id']);
            $table->dropForeign(['webinar_id']);
        });
        Schema::dropIfExists('lecturer_webinar');
    }
}
