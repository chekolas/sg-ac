<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrackUploadPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('track_upload', function (Blueprint $table) {
            $table->unsignedBigInteger('track_id')->index();
            $table->unsignedBigInteger('upload_id')->index();
        });

        Schema::table('track_upload', function (Blueprint $table) {
            $table->foreign('track_id')
                ->on('tracks')
                ->references('id');
            $table->foreign('upload_id')
                ->on('uploads')
                ->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('track_upload', function (Blueprint $table) {
            $table->dropIndex(['track_id']);
            $table->dropIndex(['upload_id']);
            $table->dropForeign(['track_id']);
            $table->dropForeign(['upload_id']);
        });
        Schema::dropIfExists('track_upload');
    }
}
