<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnswerUploadPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer_upload', function (Blueprint $table) {
            $table->unsignedBigInteger('answer_id')->index();
            $table->unsignedBigInteger('upload_id')->index();
        });

        Schema::table('answer_upload', function (Blueprint $table) {
            $table->foreign('answer_id')
                ->on('answers')
                ->references('id');
            $table->foreign('upload_id')
                ->on('uploads')
                ->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('answer_upload', function (Blueprint $table) {
            $table->dropIndex(['answer_id']);
            $table->dropIndex(['upload_id']);
            $table->dropForeign(['answer_id']);
            $table->dropForeign(['upload_id']);
        });
        Schema::dropIfExists('answer_upload');
    }
}
