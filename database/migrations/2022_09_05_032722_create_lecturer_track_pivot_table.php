<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLecturerTrackPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lecturer_track', function (Blueprint $table) {
            $table->unsignedBigInteger('lecturer_id')->index();
            $table->unsignedBigInteger('track_id')->index();
        });

        Schema::table('lecturer_track', function (Blueprint $table) {
            $table->foreign('lecturer_id')
                ->references('id')
                ->on('lecturers');
            $table->foreign('track_id')
                ->references('id')
                ->on('tracks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lecturer_track', function (Blueprint $table) {
            $table->dropIndex(['lecturer_id']);
            $table->dropIndex(['track_id']);
            $table->dropForeign(['lecturer_id']);
            $table->dropForeign(['track_id']);
        });
        Schema::dropIfExists('lecturer_track');
    }
}
