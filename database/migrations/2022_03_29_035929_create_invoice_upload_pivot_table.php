<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceUploadPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_upload', function (Blueprint $table) {
            $table->unsignedBigInteger('invoice_id')->index();
            $table->unsignedBigInteger('upload_id')->index();
            $table->timestamps();
        });

        Schema::table('invoice_upload', function (Blueprint $table) {
            $table->foreign('invoice_id')
                ->references('id')
                ->on('invoices');
            $table->foreign('upload_id')
                ->references('id')
                ->on('uploads');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice_upload', function (Blueprint $table) {
            $table->dropIndex(['invoice_id']);
            $table->dropIndex(['upload_id']);
            $table->dropForeign(['invoice_id']);
            $table->dropForeign(['upload_id']);
        });
        Schema::dropIfExists('invoice_upload');
    }
}
