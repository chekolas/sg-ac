<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryLecturerPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_lecturer', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->index();
            $table->unsignedBigInteger('lecturer_id')->index();
        });
        Schema::table('category_lecturer', function (Blueprint $table) {
            $table->foreign('category_id')
                ->references('id')
                ->on('categories');
            $table->foreign('lecturer_id')
                ->references('id')
                ->on('lecturers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_lecturer', function (Blueprint $table) {
            $table->dropIndex(['category_id']);
            $table->dropIndex(['lecturer_id']);
            $table->dropForeign(['category_id']);
            $table->dropForeign(['lecturer_id']);
        });
        Schema::dropIfExists('category_lecturer');
    }
}
