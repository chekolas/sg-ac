<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryContentPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_content', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->index();
            $table->unsignedBigInteger('content_id')->index();
        });
        Schema::table('category_content', function (Blueprint $table) {
            $table->foreign('category_id')
                ->references('id')
                ->on('categories');
            $table->foreign('content_id')
                ->references('id')
                ->on('contents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_content', function (Blueprint $table) {
            $table->dropIndex(['category_id']);
            $table->dropIndex(['content_id']);
            $table->dropForeign(['category_id']);
            $table->dropForeign(['content_id']);
        });
        Schema::dropIfExists('category_content');
    }
}
