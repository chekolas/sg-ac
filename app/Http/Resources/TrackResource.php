<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TrackResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'status' => $this->status,
            'contents' => ContentResource::collection($this->whenLoaded('contents')),
            'questions' => QuestionResource::collection($this->whenLoaded('questions')),
            'lecturers' => LectureResource::collection($this->whenLoaded('lecturers')),
            'categories' => CategoryResource::collection($this->whenLoaded('categories')),
            'uploads' => UploadResource::collection($this->whenLoaded('uploads')),
            'product' => new ProductResource($this->whenLoaded('product')),
        ];
    }
}
