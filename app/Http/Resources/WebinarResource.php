<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WebinarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'started_at' => $this->started_at,
            'ended_at' => $this->ended_at,
            'status' => $this->status,
            'venue' => $this->venue,
            'url' => $this->url,
            'code' => $this->code,
            'capacity' => $this->capacity,
            'users_count' => $this->when(!!$this->users_count, $this->users_count),
            'product' => new ProductResource($this->whenLoaded('product')),
            'uploads' => UploadResource::collection($this->whenLoaded('uploads')),
            'categories' => CategoryResource::collection($this->whenLoaded('categories')),
            'lecturers' => LectureResource::collection($this->whenLoaded('lecturers')),
            'users' => UserResource::collection($this->whenLoaded('users'))
        ];
    }
}
