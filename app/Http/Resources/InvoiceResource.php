<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'amount' => $this->amount,
            'status' => $this->status,
            'user' => $this->user,
            'products' => $this->when($this->data, $this->products),
            'vouchers' => VoucherResource::collection($this->whenLoaded('vouchers')),
            'uploads' => UploadResource::collection($this->whenLoaded('uploads'))
        ];
    }
}
