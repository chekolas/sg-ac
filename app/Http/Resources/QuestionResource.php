<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class QuestionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'description' => $this->description,
            'grade' => $this->grade,
            'time' => $this->whenPivotLoaded('content_question', function () {
                return $this->pivot->time;
            }),
            'answers' => $this->when($this->type === 1, AnswerResource::collection($this->whenLoaded('answers'))),
            'choices' => $this->when($this->type === 0, AnswerResource::collection($this->whenLoaded('answers'))),
            'categories' => CategoryResource::collection($this->whenLoaded('categories')),
            'uploads' => UploadResource::collection($this->whenLoaded('uploads'))
        ];
    }
}
