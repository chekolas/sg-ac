<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserQuestionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'question_id' => $this->question_id,
            'answer_id' => $this->answer_id,
            'grade' => $this->grade,
            'question' => new QuestionResource($this->whenLoaded('question')),
            'answer' => $this->when($this->answer, function () {
                return new AnswerResource($this->answer);
            })
        ];
    }
}
