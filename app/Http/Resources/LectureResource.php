<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LectureResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'avatar' => $this->avatar,
            'abstract' => $this->abstract,
            'description' => $this->description,
            'user' => new UserResource($this->whenLoaded('user')),
            'uploads' => UploadResource::collection($this->whenLoaded('uploads')),
            'products_count' => $this->products_count,
            'categories_count' => $this->categories_count,
            'contents_count' => $this->contents_count
        ];
    }
}
