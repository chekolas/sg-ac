<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'avatar' => $this->avatar,
            'name' => $this->name,
            'abstract' => $this->abstract,
            'description' => $this->description,
            'price' => $this->price,
            'normal' => $this->normal,
            'discount' => $this->discount,
            'special' => $this->special,
            'status' => $this->status,
            'uploads' => UploadResource::collection($this->whenLoaded('uploads')),
            'categories' => CategoryResource::collection($this->whenLoaded('categories')),
            'lecturers' => LectureResource::collection($this->whenLoaded('lecturers')),
            'bootcamp' => $this->when($this->isBootcamp(), function () {
                return WebinarResource::collection($this->whenLoaded('webinars'));
            }),
            'webinar' => $this->when($this->isWebinar(), function () {
                return new WebinarResource ($this->webinars->first());
            }),
            'track' => new TrackResource($this->whenLoaded('track'))
        ];
    }
}
