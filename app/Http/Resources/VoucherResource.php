<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VoucherResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'code' => $this->code,
            'amount' => $this->amount,
            'limit' => $this->limit,
            'description' => $this->description,
            'started_at' => $this->started_at,
            'expired_at' => $this->expired_at,
            'status' => $this->status,
            'products_count' => $this->when($request->user('api')->hasRole('administrator'), $this->products_count),
            'users_count' => $this->when($request->user('api')->hasRole('administrator'), $this->users_count),
            'products' => ProductResource::collection($this->whenLoaded('products')),
            'users' => UserResource::collection($this->whenLoaded('users'))
        ];
    }
}
