<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'color' => $this->color,
            'text_color' => $this->text_color,
            'avatar' => $this->avatar,
            'description' => $this->description,
            'uploads' => UploadResource::collection($this->whenLoaded('uploads'))
        ];
    }
}
