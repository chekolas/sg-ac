<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'url' => $this->url,
            'duration' => $this->duration,
            'page' => $this->whenPivotLoaded('content_track', function () {
                return $this->pivot->page;
            }),
            'lecturers' => LectureResource::collection($this->whenLoaded('lecturers')),
            'categories' => CategoryResource::collection($this->whenLoaded('categories')),
            'questions' => QuestionResource::collection($this->whenLoaded('questions')),
            'uploads' => UploadResource::collection($this->whenLoaded('uploads'))
        ];
    }
}
