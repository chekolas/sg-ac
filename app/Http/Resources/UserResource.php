<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'avatar' => $this->avatar,
            'name' => $this->name,
            'status' => $this->status,
            'phone' => $this->when($request->user()->hasRole('administrator'), $this->phone),
            'email' => $this->when($request->user()->hasRole('administrator'), $this->email),
            'products' => ProductResource::collection($this->whenLoaded('products')),
            'webinars' => WebinarResource::collection($this->whenLoaded('webinars')),
            'vouchers' => $this->whenLoaded('vouchers', function () {
                return VoucherResource::collection($this->availableVouchers);
            })
        ];
    }
}
