<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\InvoiceResource;
use App\Invoice;
use App\Product;
use App\Voucher;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class InvoiceController extends Controller
{
    private $user;

    public function __construct()
    {
        $this->user = auth('api')->user();
    }

    //
    public function index(Request $request)
    {
        if ($request->filled('id')) {
            $item = Invoice::find($request->input('id'));
            return new InvoiceResource($item);
        } else {
            $items = Invoice::where('user_id', $this->user->id);
            if ($request->filled('sortBy')) {
                $field = $request->input('sortBy');
                $order = $request->input('descending') === 'true' ? 'desc' : 'asc';
                $items->orderBy($field, $order);
            }

            if ($request->filled('rowsPerPage')) {
                $perPage = $request->input('rowsPerPage');
                $data = $perPage > 0 ? $items->paginate($perPage) : $items->get();
            } else {
                $data = $items->get();
            }

            InvoiceResource::wrap('results');
            return InvoiceResource::collection($data);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'products' => 'required|array'
        ]);

        if ($request->filled('id')) {
            $item = Invoice::find($request->input('id'));
        } else {
            $item = new Invoice();
            $item->user()->associate($this->user);
        }

        $productIDs = Arr::pluck($request->input('products'), 'id');
        $products = Product::find($productIDs);

        if ($request->filled('vouchers')) {
            // collecting vouchers
            $vouchersIDs = Arr::pluck($request->input('vouchers'), 'id');
            $vouchers = Voucher::with(['products'])
                ->find($vouchersIDs);
        }
    }
}
