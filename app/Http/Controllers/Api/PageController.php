<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PageResource;
use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index($slug)
    {
        $page = Page::where('slug', '=', $slug)->first();
        if ($page) {
            return new PageResource($page);
        } else {
            return response()->json([
                'message' => 'Page Not Found!'
            ], 404);
        }
    }
}
