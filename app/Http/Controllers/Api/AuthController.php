<?php

namespace App\Http\Controllers\Api;

use App\Events\UserRegisteredEvent;
use App\Http\Controllers\Controller;
use App\Http\Resources\InvoiceResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    public function username()
    {
        return 'phone';
    }

    public function login(Request $request)
    {
        $request->validate([
            'phone' => 'required',
            'password' => 'required'
        ]);

        $credentials = $request->only(['phone', 'password']);

        if (! $token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function register(Request $request)
    {
        $request->validate([
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|unique:users,phone',
            'password' => 'required|min:8',
            'name' => 'required'
        ]);

        $name = $request->input('name');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $password = $request->input('password');

        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->phone = $phone;
        $user->password = Hash::make($password);
        $user->save();

        event(new UserRegisteredEvent($user));

        return $this->respondWithToken(auth('api')->login($user));
    }

    public function verify(Request $request)
    {
        $request->validate([
            'code' => 'required|min:6'
        ]);

        $code = $request->input('code');
        $user = User::find(auth('api')->id());

        if ($user->code === $code) {
            $user->status = User::ACTIVE;
            $user->email_verified_at = Carbon::now();
            $user->save();
            return response()->json($this->transform($user));
        } else {
            return response()->json([
                'message' => 'invalid verification code'
            ], 403);
        }
    }

    public function refresh()
    {
        return $this->respondWithToken(auth('api')->refresh());
    }

    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function me()
    {
        return response()->json(
            $this->transform(User::find(auth('api')->id()))
        );
    }

    public function invoices()
    {
        $user = auth('api')->user();
        $invoices = $user->invoices()->where('invoices.status', '=', 0)->get();

        return InvoiceResource::collection($invoices);
    }

    protected function transform(User $user)
    {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'avatar' => $user->avatar ? $user->avatar : Storage::url('student.png'),
            'email' => $user->email,
            'phone' => $user->phone,
            'status' => $user->status,
            'roles' => $user->getRoleNames()
        ];
    }
}
