<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    //
    public function index(Request $request)
    {
        if ($request->filled('id')) {
            $item = Product::find($request->input('id'));
            return new ProductResource($item);
        } else {
            $items = Product::with('vouchers');
            if ($request->filled('sortBy')) {
                $field = $request->input('sortBy');
                $order = $request->input('descending') === 'true' ? 'desc' : 'asc';
                $items->orderBy($field, $order);
            }

            if ($request->filled('type')) {
                $type = $request->input('type');

                switch ($type) {
                    case 'webinar':
                        $items->has('webinars', '<', 2);
                        break;
                    case 'track':
                        $items->has('track');
                        break;
                    case 'bootcamp':
                        $items->has('webinars', '>', 1);
                        break;
                    default:
                        break;
                }
            }

            if ($request->filled('categories')) {
                Log::info('get product by categories', [$request->input('categories')]);
            }

            if ($request->filled('rowsPerPage')) {
                $perPage = $request->input('rowsPerPage');
                $data = $perPage > 0 ? $items->paginate($perPage) : $items->get();
            } else {
                $data = $items->get();
            }

            ProductResource::wrap('results');
            return ProductResource::collection($data);
        }
    }
}
