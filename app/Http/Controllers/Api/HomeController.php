<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    public function getTopBanners()
    {
        return response()->json(json_decode(Storage::get('top_banners.json')));
    }

    public function getSubBanners()
    {
        return response()->json(json_decode(Storage::get('sub_banners.json')));
    }

    public function getBody()
    {
        return response()->json(json_decode(Storage::get('body.json')));
    }

    public function getHeaderMenus()
    {
        return response()->json(json_decode(Storage::get('header-menus.json')));
    }

    public function getFooterMenus()
    {
        return response()->json(json_decode(Storage::get('footer-menus.json')));

    }
}
