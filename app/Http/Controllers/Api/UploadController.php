<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UploadResource;
use App\Upload;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UploadController extends Controller
{
    private $user;

    public function __construct()
    {
        $this->user = auth('api')->user();
    }
    public function index(Request $request)
    {

    }

    public function store(Request $request)
    {
        $files = $request->file('upload');
        if (is_array($files)) {
            return UploadResource::collection($this->multiFiles($files));
        } else {
            return new UploadResource($this->singleFile($files));
        }
    }

    private function singleFile(UploadedFile $file)
    {
        $name = Str::kebab($file->getClientOriginalName());
        Log::alert('string kebab', [$name]);
        $filename =  $this->user->id .
        '-' . Carbon::now()->timestamp .
        '-' . $name;

        $path = Storage::putFileAs('uploads', $file, $filename);

        $upload = new Upload();
        $upload->path = $path;
        $upload->mime = $file->getClientMimeType();
        $upload->meta = [
            'name' => $filename,
            'size' => Storage::size($path)
        ];
        $upload->uploader()->associate($this->user);
        $upload->save();

        return $upload;
    }

    private function multiFiles($files)
    {
        $uploads = collect();

        foreach ($files as $file) {
            Log::alert('uploaded each', [$file]);
            $uploads->push($this->singleFile($file));
        }

        return $uploads;
    }
}
