<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Voucher;
use Illuminate\Http\Request;

class VoucherController extends Controller
{
    //
    public function index(Request $request)
    {
        if ($request->filled('code')) {
            $item = Voucher::where('code', '=', $request->input('code'))->first();
            if ($user = auth('api')->user()) {
                $item->whereHas('users', function ($q) use ($user) {
                    $q->find($user->id);
                });
            }
        }
    }
}
