<?php

namespace App\Http\Controllers\Admin;

use App\Answer;
use App\Http\Controllers\Controller;
use App\Http\Resources\AnswerResource;
use App\Http\Resources\QuestionResource;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class QuestionController extends Controller
{
    public function index(Request $request)
    {
        if ($request->filled('id')) {
            $item = Question::find($request->input('id'))->load(['answers', 'answers.uploads', 'categories', 'uploads']);
            return new QuestionResource($item);
        } else {
            $items = Question::with(['answers', 'categories', 'uploads']);

            if ($request->filled('search')) {
                $q = $request->input('search');
                $items->where('description', 'ilike', "%$q%");
            }

            if ($request->filled('sortBy')) {
                $sort = $request->input('sortBy');
                $order = $request->input('descending') === 'true' ? 'desc' : 'asc';
                $items->orderBy($sort, $order);
            } else {
                $items->latest();
            }

            if ($request->filled('rowsPerPage')) {
                $perPage = $request->input('rowsPerPage');
                $data = $perPage > 0 ? $items->paginate($perPage) : $items->get();
            } else {
                $data = $items->get();
            }

            QuestionResource::wrap('results');
            return QuestionResource::collection($data);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'type' => 'required',
            'description' => 'required'
        ]);

        if ($request->filled('id')) {
            $item = Question::find($request->input('id'));
        } else {
            $item = new Question();
        }

        $item->type = $request->input('type');
        $item->description = $request->input('description');

        if ($request->filled('grade')) {
            $item->grade = $request->input('grade');
        }

        if ($item->save()) {
            if ($request->filled('categories')) {
                $categories = Arr::pluck($request->input('categories'), 'id');
                $item->categories()->sync($categories);
                $item->load('categories');
            }
            if ($request->filled('uploads')) {
                $uploads = Arr::pluck($request->input('uploads'), 'id');
                $item->uploads()->sync($uploads);
                $item->load('uploads');
            }
            if ($request->filled('answers')) {
                $answers = Answer::find(Arr::pluck($request->input('answers'), 'id'));
                foreach ($answers as $answer) {
                    $answer->question()->dissociate();
                    $answer->save();
                }
                $item->answers()->saveMany($answers);
                $item->load('answers');
            }
            return new QuestionResource($item);
        }
    }

    public function answer(Request $request)
    {
        $request->validate([
            'description' => 'required',
            'status' => 'required'
        ]);

        if ($request->filled('id')) {
            $item = Answer::find($request->input('id'));
        } else {
            $item = new Answer();
        }

        $item->description = $request->input('description');
        $item->status = $request->input('status');

        if ($item->save()) {
            return new AnswerResource($item);
        } else {
            return response()->json([
                'message' => 'Ops.. something went wrong!'
            ], 500);
        }
    }
}
