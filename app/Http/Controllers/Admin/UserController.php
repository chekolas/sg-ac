<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if ($request->filled('id')) {
            $item = User::with(['webinars', 'tracks', 'tracks.questions', 'vouchers', 'invoices'])
                ->find($request->input('id'));

            return new UserResource($item);
        } else {
            $items = User::withCount(['webinars', 'tracks', 'questions', 'invoices', 'vouchers']);
            if ($request->filled('search')) {
                $q = $request->input('search');
                $items->where('name', 'ilike', "%$q%")
                    ->orWhere('phone', 'ilike', "%$q%")
                    ->orWhere('email', 'ilike', "%$q%");
            }

            if ($request->filled('sortBy')) {
                $field = $request->input('sortBy');
                $order = $request->input('descending') === 'true' ? 'desc' : 'asc';
                $items->orderBy($field, $order);
            } else {
                $items->latest();
            }

            if ($request->filled('rowsPerPage')) {
                $perPage = $request->input('rowsPerPage');
                $data = $perPage > 0 ? $items->paginate($perPage) : $items->get();
            } else {
                $data = $items->get();
            }

            UserResource::wrap('results');
            return UserResource::collection($data);
        }
    }
}
