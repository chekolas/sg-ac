<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Product;
use App\Track;
use App\Webinar;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        if ($request->filled('id')) {
            $item = Product::withCount(['vouchers', 'users'])
                ->with(['uploads', 'categories', 'lecturers', 'webinars', 'track'])
                ->find($request->input('id'));

            return new ProductResource($item);
        } else {
            $items = Product::withCount(['vouchers', 'users'])
                ->with(['uploads', 'categories', 'lecturers']);

            if ($request->filled('type')) {
                $type = $request->input('type');
                switch ($type) {
                    case 'bootcamp':
                        $items->has('webinars', '>', 1)->with(['webinars']);
                        break;
                    case 'webinar':
                        $items->has('webinars', '=', 1)->with(['webinars']);
                        break;
                    case 'track':
                        $items->has('track')->with('track');
                        break;
                }
            }

            if ($request->filled('search')) {
                $q = $request->input('search');
                $items->where('name', 'ilike', "%$q%")
                    ->orWhere('abstract', 'ilike', "%$q%");
            }

            if ($request->filled('sortBy')) {
                $sort = $request->input('sortBy');
                $order = $request->input('descending') === 'true' ? 'desc' : 'asc';
                $items->orderBy($sort, $order);
            } else {
                $items->latest();
            }

            if ($request->filled('rowsPerPage')) {
                $perPage = $request->input('rowsPerPage');
                $data = $perPage > 0 ? $items->paginate($perPage) : $items->get();
            } else {
                $data = $items->get();
            }

            ProductResource::wrap('results');
            return ProductResource::collection($data);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'abstract' => 'required|string',
            'description' => 'required',
            'normal' => 'required|numeric',
        ]);

        if ($request->filled('id')) {
            $product = Product::find($request->input('id'));
        } else {
            $product = new Product();
        }

        $product->name = $request->input('name');
        $product->abstract = $request->input('abstract');
        $product->description = $request->input('description');
        $product->normal = $request->input('normal');
        $product->avatar = $request->filled('avatar') ? $request->input('avatar') : null;
        $product->discount = $request->filled('discount') ? $request->input('discount') : null;
        $product->special = $request->filled('special') ? $request->input('special') : null;
        $product->status = $request->filled('status') ? $request->input('status') : 0;

        if ($product->save()) {
            if ($request->filled('uploads')) {
                $uploads = Arr::pluck($request->input('uploads'), 'id');
                $product->uploads()->sync($uploads);
                $product->load('uploads');
            }
            if ($request->filled('lecturers')) {
                $lecturers = Arr::pluck($request->input('lecturers'), 'id');
                $product->lecturers()->sync($lecturers);
                $product->load('lecturers');
            }
            if ($request->filled('categories')) {
                $categories = Arr::pluck($request->input('categories'), 'id');
                $product->categories()->sync($categories);
                $product->load('categories');
            }
            if ($request->filled('bootcamp')) {
                $webinars = Webinar::find(Arr::pluck($request->input('bootcamp'), 'id'));
                $product->webinars()->saveMany($webinars);
                $product->load('webinars');
            }
            if ($request->filled('track.id')) {
                $track = Track::find($request->input('track.id'));
                if ($request->filled('track.contents')) {
                    $contents = $request->input('track.contents');
                    $cs = [];
                    foreach ($contents as $index => $content) {
                        $cs[$content['id']] = [
                            'page' => $index + 1
                        ];
                    }
                    $track->contents()->sync($cs);
                }
                if ($request->filled('track.questions')) {
                    $questions = $request->input('track.questions');
                    $qs = [];
                    foreach ($questions as $index => $question) {
                        $qs[$question['id']] = [
                            'page' => $index + 1
                        ];
                    }
                    $track->questions()->sync($qs);
                }
                $product->track()->save($track);
                $product->load(['track', 'track.contents', 'track.questions']);
            }
            return new ProductResource($product);
        }
        return response()->json([
            'message' => 'Ops..!! something went wong'
        ], 500);
    }
}
