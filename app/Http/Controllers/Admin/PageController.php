<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\PageResource;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class PageController extends Controller
{
    public function index(Request $request)
    {
        if ($request->filled('id')) {
            $id = $request->input('id');
            $item = Page::with(['user', 'categories','uploads'])
                ->find($id);
            return $item ? new PageResource($item) : response()->json([
                'message' => ' WTF DUDE!!!'
            ], 404);
        } else if ($request->filled('slug')) {
            $slug = $request->input('slug');
            $item = Page::with(['user', 'categories','uploads'])
                ->where('slug', '=', $slug)
                ->first();
            return $item ? new PageResource($item) : response()->json([
                'message' => ' WTF DUDE!!!'
            ], 404);
        } else {
            $items = new Page();
            if ($request->filled('search')) {
                $q = $request->input('search');
                $items->where('title', 'ilike', "%$q%")
                    ->orWhere('abstract', 'ilike', "%$q%");
            }

            if ($request->filled('sortBy')) {
                $sort = $request->input('sortBy');
                $order = $request->input('descending') === 'true' ? 'desc' : 'asc';
                $items->orderBy($sort, $order);
            } else {
                $items->latest();
            }

            if ($request->filled('rowsPerPage')) {
                $rows = $request->input('rowsPerPage');
                $data = $rows > 0 ? $items->paginate($rows) : $items->get();
            } else {
                $data = $items->get();
            }

            PageResource::wrap('results');
            return PageResource::collection($data);
        }
    }
    public function store (Request $request)
    {
        $request->validate([
            'title' => 'required',
            'data' => 'required',
            'status' => 'required'
        ]);

        $title = $request->input('title');
        $data = $request->input('data');

        if ($request->filled('id')) {
            $item = Page::find($request->input('id'));
        } else {
            $item = new Page();
            $slug = $request->filled('slug') ? $request->input('slug') : false;
            if (!$slug) {
                $slug = Str::slug($request->input('title'));
            }
            if ($slug && Page::where('slug', '=', $slug)->exists()) {
                $slug = now()->format('U') . '-' . $slug;
            }
            $item->slug = $slug;
        }

        $item->title = $title;
        $item->data = json_encode($data);
        $item->status = $request->input('status');
        $item->user()->associate(auth('api')->user());

        if ($item->save()) {
            if ($request->filled('uploads')) {
                $uploads = Arr::pluck($request->input('uploads'), 'id');
                $item->uploads()->sync($uploads);
                $item->load('uploads');
            }

            if ($request->filled('categories')) {
                $categories = Arr::pluck($request->input('categories'), 'id');
                $item->categories()->sync($categories);
                $item->load('categories');
            }

            return new PageResource($item);
        } else {
            return response()->json([
                'message' => 'Ops.. something went wrong!'
            ], 500);
        }
    }
}
