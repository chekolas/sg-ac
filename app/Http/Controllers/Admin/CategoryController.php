<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        if ($request->filled('id')) {
            $item = Category::find($request->input('id'));
            return new CategoryResource($item);
        } else {
            $items = Category::withCount(['products']);

            if ($request->filled('search')) {
                $q = $request->input('search');
                $items->where('name', 'ilike', "%$q%")
                    ->orWhere('description', 'ilike', "%$q%");
            }

            if ($request->filled('sortBy')) {
                $field = $request->input('sortBy');
                $order = $request->input('descending') === 'true' ? 'desc' : 'asc';
                $items->orderBy($field, $order);
            } else {
                $items->latest();
            }

            if ($request->filled('rowsPerPage')) {
                $perPage = $request->input('rowsPerPage');
                $data = $perPage > 0 ? $items->paginate($perPage) : $items->get();
            } else {
                $data = $items->get();
            }

            CategoryResource::wrap('results');
            return CategoryResource::collection($data);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        if ($request->filled('id')) {
            $item = Category::find($request->input('id'));
        } else {
            $item = new Category();
        }

        $item->name = $request->input('name');
        $item->avatar = $request->input('avatar');
        $item->description = $request->input('description');

        if ($request->filled('color')) {
            $item->color = $request->input('color');
        }

        if ($request->filled('text_color')) {
            $item->text_color = $request->input('text_color');
        }

        if ($item->save()) {
            if ($request->filled('uploads')) {
                $uploads = Arr::pluck($request->input('uploads'), 'id');
                $item->uploads()->sync($uploads);
                $item->load('uploads');
            }
        }

        return new CategoryResource($item->loadCount(['products', 'lecturers']));
    }
}
