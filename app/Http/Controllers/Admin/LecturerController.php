<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\LectureResource;
use App\Lecturer;
use App\Upload;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class LecturerController extends Controller
{
    //
    public function index(Request $request)
    {
        if ($request->filled('id')) {
            $item = Lecturer::find($request->input('id'));
            return new LectureResource($item);
        } else {
            $items = Lecturer::with(['products', 'categories', 'contents']);

            if ($request->filled('search')) {
                $q = $request->input('search');
                $items->where('name', 'ilike', "%$q%")
                    ->orWhere('abstract', 'ilike', "%$q%");
            }

            if ($request->filled('sortBy')) {
                $field = $request->input('sortBy');
                $order = $request->input('descending') === 'true' ? 'desc' : 'asc';
                $items->orderBy($field, $order);
            } else {
                $items->latest();
            }

            if ($request->filled('rowsPerPage')) {
                $row = $request->input('rowsPerPage');
                $data = $row > 0 ? $items->paginate($row) : $items->get();
            } else {
                $data = $items->get();
            }

            LectureResource::wrap('results');
            return LectureResource::collection($data);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        if ($request->filled('id')) {
            $item = Lecturer::find($request->input('id'));
        } else {
            $item = new Lecturer();
        }

        $item->name = $request->input('name');
        $item->avatar = $request->input('avatar');
        $item->abstract = $request->input('abstract');
        $item->description = $request->input('description');

        if ($item->save()) {
            if ($request->filled('uploads')) {
                $uploads = Arr::pluck($request->input('uploads'), 'id');
                $item->uploads()->sync($uploads);
                $item->load('uploads');
            }
            if ($request->filled('user')) {
                $user = User::find($request->input('user.id'));
                $item->user()->associate($user);
                $item->load('user');
            }
            if ($request->filled('categories')) {
                $categories = Arr::pluck($request->input('categories'), 'id');
                $item->categories()->sync($categories);
                $item->load('categories');
            }
            return new LectureResource($item);
        }

        return response()->json([
            'message' => 'Ops!! something went wrong!'
        ], 500);
    }
}
