<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\TrackResource;
use App\Product;
use App\Track;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class TrackController extends Controller
{
    public function index(Request $request)
    {
        if ($request->filled('id')) {
            $item = Track::find($request->input('id'));
            return new TrackResource($item->load([
                'categories',
                'lecturers',
                'uploads',
                'questions',
                'contents',
                'contents.uploads',
                'contents.categories',
                'contents.lecturers',
                'contents.questions'
            ]));
        } else {
            $items = Track::with('product')
                ->withCount(['contents', 'users'])
                ->latest();

            if ($request->filled('search')) {
                $q = $request->input('search');
                $items->where('name', 'like', "%$q")
                    ->orWhere('description', 'like', "%$q%");
            }

            if ($request->filled('sortBy')) {
                $sort = $request->input('sortBy');
                $order = $request->input('descending') === 'true' ? 'desc' : 'asc';
                $items->orderBy($sort, $order);
            } else {
                $items->latest();
            }

            if ($request->filled('rowsPerPage')) {
                $perPage = $request->input('rowsPerPage');
                $data = $perPage > 0 ? $items->paginate($perPage) : $items->get();
            } else {
                $data = $items->get();
            }

            TrackResource::wrap('results');
            return TrackResource::collection($data);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required'
        ]);

        $name = $request->input('name');
        $desc = $request->input('description');

        if ($request->filled('id')) {
            $item = Track::find($request->input('id'));
        } else {
            $item = new Track();
        }

        $item->name = $name;
        $item->description = $desc;
        $item->status = $request->filled('status') ? $request->input('status') : 0;

        if ($request->filled('product.id')) {
            $product = Product::find($request->input('product.id'));
            $item->product()->associate($product);
        }

        if ($item->save()) {
            if ($request->filled('uploads')) {
                $uploads = Arr::pluck($request->input('uploads'), 'id');
                $item->uploads()->sync($uploads);
                $item->load('uploads');
            }

            if ($request->filled('categories')) {
                $categories = Arr::pluck($request->input('categories'), 'id');
                $item->categories()->sync($categories);
                $item->load('categories');
            }

            if ($request->filled('lecturers')) {
                $lecturers = Arr::pluck($request->input('lecturers'), 'id');
                $item->lecturers()->sync($lecturers);
                $item->load('lecturers');
            }

            if ($request->filled('contents')) {
                $contents = $request->input('contents');
                $cs = [];
                foreach ($contents as $index => $content) {
                    $cs[$content['id']] = [
                        'page' => $index + 1
                    ];
                }
                $item->contents()->sync($cs);
                $item->load('contents');
            }

            if ($request->filled('questions')) {
                $questions = Arr::pluck($request->input('questions'), 'id');
                $item->questions()->sync($questions);
                $item->load('questions');
            }

            return new TrackResource($item);
        }
    }
}
