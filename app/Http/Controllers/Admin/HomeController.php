<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    public function setTopBanners(Request $request)
    {
        $request->validate([
            'data' => 'required'
        ]);

        $data = json_encode($request->input('data'));

        if (Storage::put('top_banners.json', $data)) {
            return response()->json([
                'message' => 'success'
            ]);
        }

        return response()->json([
            'message' => 'failed'
        ], 500);
    }

    public function setSubBanners(Request $request)
    {
        $request->validate([
            'data' => 'required'
        ]);

        $data = json_encode($request->input('data'));

        if (Storage::put('sub_banners.json', $data)) {
            return response()->json([
                'message' => 'success'
            ]);
        }

        return response()->json([
            'message' => 'failed'
        ], 500);
    }

    public function setBody(Request $request)
    {
        $request->validate([
            'data' => 'required'
        ]);

        $data = json_encode($request->input('data'));

        if (Storage::put('body.json', $data)) {
            return response()->json([
                'message' => 'success'
            ]);
        }

        return response()->json([
            'message' => 'failed'
        ], 500);
    }

    public function setHeaderMenu(Request $request)
    {
        $request->validate(['data' => 'required']);

        $data = json_encode($request->input('data'));
        if (Storage::put('header-menus.json', $data)) {
            return response()->json([
                'message' => 'success'
            ]);
        }

        return response()->json([
            'message' => 'failed'
        ], 500);
    }

    public function setFooterMenu(Request $request)
    {
        $request->validate(['data' => 'required']);

        $data = json_encode($request->input('data'));
        if (Storage::put('footer-menus.json', $data)) {
            return response()->json([
                'message' => 'success'
            ]);
        }

        return response()->json([
            'message' => 'failed'
        ], 500);
    }
}
