<?php

namespace App\Http\Controllers\Admin;

use App\Content;
use App\Http\Controllers\Controller;
use App\Http\Resources\ContentResource;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class ContentController extends Controller
{
    //
    public function index(Request $request)
    {
        if ($request->filled('id')) {
            $item = Content::find($request->input('id'))->load([
                'questions',
                'questions.answers',
                'tracks',
                'uploads',
                'categories',
                'lecturers'
            ]);
            return new ContentResource($item);
        } else {
            $items = Content::with([
                'questions',
                'uploads',
                'categories',
                'lecturers'
            ]);

            if ($request->filled('search')) {
                $q = $request->input('search');
                $items->Where('description', 'like', "%$q%");
            }

            if ($request->filled('sortBy')) {
                $sort = $request->input('sortBy');
                $order = $request->input('descending') === 'true' ? 'desc' : 'asc';
                $items->orderBy($sort, $order);
            } else {
                $items->latest();
            }

            if ($request->filled('rowsPerPage')) {
                $perPage = $request->input('rowsPerPage');
                $data = $perPage > 0 ? $items->paginate($perPage) : $items->get();
            } else {
                $data = $items->get();
            }
            ContentResource::wrap('results');
            return ContentResource::collection($data);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required'
        ]);

        if ($request->filled('id')) {
            $item = Content::find($request->input('id'));
        } else {
            $item = new Content();
        }

        $item->name = $request->input('name');
        $item->description = $request->input('description');
        $item->url = $request->filled('url') ? $request->input('url') : null;
        $item->duration = $request->filled('duration') ? $request->input('duration') : null;

        if ($item->save()) {
            if ($request->filled('track')) {
                $item->tracks()->attach($request->input('track.id'));
            }

            if ($request->filled('uploads')) {
                $uploads = Arr::pluck($request->input('uploads'), 'id');
                $item->uploads()->sync($uploads);
                $item->load('uploads');
            }

            if ($request->filled('categories')) {
                $categories = Arr::pluck($request->input('categories'), 'id');
                $item->categories()->sync($categories);
                $item->load('categories');
            }

            if ($request->filled('lecturers')) {
                $lecturers = Arr::pluck($request->input('lecturers'), 'id');
                $item->lecturers()->sync($lecturers);
                $item->load('lecturers');
            }

            if ($request->filled('questions')) {
                $questions = $request->input('questions');
                $arr = [];
                foreach ($questions as $question) {
                    $arr[$question['id']] = ['time' => $question['time']];
                }
                $item->questions()->sync($arr);
                $item->load('questions');
            }
            return new ContentResource($item);
        }
    }
}
