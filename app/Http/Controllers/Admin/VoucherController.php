<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\VoucherResource;
use App\Product;
use App\Voucher;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;

class VoucherController extends Controller
{
    public function index(Request $request)
    {
        if ($request->filled('id')) {
            $item = Voucher::with(['products', 'users'])->find($request->input('id'));
            return new VoucherResource($item);
        } else {
            $items = Voucher::withCount(['users', 'products']);

            if ($request->filled('search')) {
                $q = $request->input('search');
                $items->where('code', 'like', "%$q%")
                    ->orWhere('description', 'like', "%$q%");
            }

            if ($request->filled('sortBy')) {
                $sort = $request->input('sortBy');
                $descending = $request->input('descending') === 'true' ? 'desc' : 'asc';
                $items->orderBy($sort, $descending);
            } else {
                $items->latest();
            }

            if ($request->filled('rowsPerPage')) {
                $perPage = $request->input('rowsPerPage');
                $data = $perPage > 0 ? $items->paginate($perPage) : $items->get();
            } else {
                $data = $items->get();
            }

            VoucherResource::wrap('results');
            return VoucherResource::collection($data);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'code' => 'required',
            'description' => 'required',
            'type' => 'required',
            'amount' => 'required',
            'limit' => 'required',
            'status' => 'required'
        ]);

        if ($request->filled('id')) {
            $item = Voucher::find($request->input('id'));
        } else {
            $item = new Voucher();
        }

        $item->code = $request->input('code');
        $item->description = $request->input('description');
        $item->type = $request->input('type');
        $item->amount = $request->input('amount');
        $item->limit = $request->input('limit');
        $item->started_at = $request->filled('started_at') ?
            Carbon::parse($request->input('started_at')) : null;
        $item->expired_at = $request->filled('expired_at') ?
            Carbon::parse($request->input('expired_at')) : null;
        $item->status = $request->input('status');

        if ($item->save()) {
            if ($request->filled('uploads')) {
                $uploads = Arr::pluck($request->input('uploads'), 'id');
                if ($uploads) $item->uploads()->sync($uploads);
            }
            if ($request->filled('products')) {
                $products = Arr::pluck($request->input('products'), 'id');
                if ($products) $item->products()->sync($products);
            }

            $item->loadMissing(['products', 'uploads']);
            return new VoucherResource($item);
        } else {
            return response()->json([
                'message' => 'storing data failed'
            ],  500);
        }
    }
}
