<?php

namespace App\Http\Controllers\Admin;

use App\Events\WebinarStoredEvent;
use App\Http\Controllers\Controller;
use App\Http\Resources\WebinarResource;
use App\Product;
use App\Webinar;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;

class WebinarController extends Controller
{
    //
    public function index(Request $request)
    {
        if ($request->input('id')) {
            $item = Webinar::with(['product', 'users', 'product.vouchers', 'product.invoice'])
                ->find($request->input('id'));
            return new WebinarResource($item);
        } else {
            $items = Webinar::with('product')
                ->withCount(['users']);

            if ($request->filled('search')) {
                $q = $request->input('search');
                $items->where('name', 'ilike', "%$q%")
                    ->orWhere('description', 'ilike', "%$q%");
            }

            if ($request->filled('sortBy')) {
                $sort = $request->input('sortBy');
                $order = $request->input('descending') === 'true' ? 'desc' : 'asc';
                $items->orderBy($sort, $order);
            } else {
                $items->latest();
            }

            if ($request->filled('rowsPerPage')) {
                $perPage = $request->input('rowsPerPage');
                $data = $perPage > 0 ? $items->paginate($perPage) : $items->get();
            } else {
                $data = $items->get();
            }

            WebinarResource::wrap('results');
            return WebinarResource::collection($data);
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required'
        ]);

        $name = $request->input('name');
        $desc = $request->input('description');

        if ($request->filled('id')) {
            $item = Webinar::find($request->input('id'));
        } else {
            $item = new Webinar();
        }

        $item->name = $name;
        $item->description = $desc;
        $item->started_at = $request->filled('started_at') ? Carbon::createFromTimestamp((int)$request->input('started_at')) : null;
        $item->ended_at = $request->filled('ended_at') ? Carbon::createFromTimestamp((int)$request->input('ended_at')) : null;
        $item->venue = $request->filled('venue') ? $request->input('venue') : null;
        $item->url = $request->filled('url') ? $request->input('url') : null;
        $item->code = $request->filled('code') ? $request->input('code') : null;
        $item->status = $request->filled('status') ? $request->input('status') : 0;

        if ($request->filled('product.id')) {
            $product = Product::find($request->input('product.id'));
            $item->product()->associate($product);
        }

        if ($item->save()) {
            if ($request->filled('uploads')) {
                $uploads = Arr::pluck($request->input('uploads'), 'id');
                $item->uploads()->sync($uploads);
                $item->load('uploads');
            }

            if ($request->filled('categories')) {
                $categories = Arr::pluck($request->input('categories'), 'id');
                $item->categories()->sync($categories);
                $item->load('categories');
            }

            if ($request->filled('lecturers')) {
                $lecturers = Arr::pluck($request->input('lecturers'), 'id');
                $item->lecturers()->sync($lecturers);
                $item->load('lecturers');
            }

            event(new WebinarStoredEvent($item));
            return new WebinarResource($item);
        } else {
            return response()->json([
                'message' => 'storing data failed'
            ],  500);
        }
    }
}
