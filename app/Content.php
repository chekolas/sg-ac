<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    public function tracks()
    {
        return $this->belongsToMany('App\Track')->withPivot(['page']);
    }

    public function questions()
    {
        return $this->belongsToMany('App\Question')->withPivot(['time']);
    }

    public function uploads()
    {
        return $this->belongsToMany('App\Upload');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function lecturers()
    {
        return $this->belongsToMany('App\Lecturer');
    }
}
