<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    const IS_USED = 1;
    const IS_AVAILABLE = 0;

    public function users()
    {
        return $this->belongsToMany('App\User', 'voucher_user')->withPivot(['is_used']);
    }

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    public function uploads()
    {
        return $this->belongsToMany('App\Upload');
    }
}
