<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lecturer extends Model
{
    //
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function uploads()
    {
        return $this->belongsToMany('App\Upload');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function contents()
    {
        return $this->belongsToMany('App\Content');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }
}
