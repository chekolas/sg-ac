<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    public function webinars()
    {
        return $this->hasMany('App\Webinar')->orderBy('started_at');
    }

    public function isBootcamp()
    {
        return $this->webinars()->count() > 1;
    }

    public function isWebinar()
    {
        return $this->webinars()->count() < 2;
    }

    public function track()
    {
        return $this->hasOne('App\Track');
    }

    public function vouchers()
    {
        return $this->belongsToMany('App\Voucher');
    }

    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot(['expired_at'])->withTimestamps();
    }

    public function uploads()
    {
        return $this->belongsToMany('App\Upload');
    }

    public function lecturers()
    {
        return $this->belongsToMany('App\Lecturer');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function getPriceAttribute()
    {
        return $this->status === 0 ? $this->normal : ($this->status === 1 ? $this->discount : $this->special);
    }
}
