<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    public function lecturers()
    {
        return $this->belongsToMany('App\Lecturer');
    }

    public function uploads()
    {
        return $this->belongsToMany('App\Upload');
    }
}
