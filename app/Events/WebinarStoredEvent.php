<?php

namespace App\Events;

use App\Http\Resources\WebinarResource;
use App\Webinar;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class WebinarStoredEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $webinar;

    /**
     * Create a new event instance.
     *
     * @param Webinar $webinar
     */
    public function __construct(Webinar $webinar)
    {
        $this->webinar = $webinar;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('webinars');
    }

    public function broadcastWith()
    {
        return (new WebinarResource($this->webinar))->resolve();
    }
}
