<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    protected $casts = [
        'meta' => 'object'
    ];

    public function uploader()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function invoices()
    {
        return $this->belongsToMany('App\Invouce');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    public function webinars()
    {
        return $this->belongsToMany('App\Webinar');
    }

    public function contents()
    {
        return $this->belongsToMany('App\Content');
    }

    public function questions()
    {
        return $this->belongsToMany('App\Question');
    }

    public function tracks()
    {
        return $this->belongsToMany('App\Track');
    }

    public function lecturers()
    {
        return $this->belongsToMany('App\Lecturer');
    }
}
