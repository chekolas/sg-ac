<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Track extends Model
{
    public function contents()
    {
        return $this->belongsToMany('App\Content')->withPivot(['page'])->orderBy('content_track.page', 'asc');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function questions()
    {
        return $this->belongsToMany('App\Question')->withPivot(['page'])->orderBy('question_track.page', 'asc');
    }

    public function uploads()
    {
        return $this->belongsToMany('App\Upload');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function lecturers()
    {
        return $this->belongsToMany('App\Lecturer');
    }
}
