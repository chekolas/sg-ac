<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    const INACTIVE = 0;
    const ACTIVE = 1;
    const BANNED = 10;
    use Notifiable, HasRoles;

    protected $guard_name = 'api';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function uploader()
    {
        return $this->hasMany('App\Upload');
    }

    public function vouchers()
    {
        return $this->belongsToMany('App\Voucher', 'voucher_user')->withPivot(['is_used']);
    }

    public function invoices()
    {
        return $this->hasMany('App\Invoice');
    }

    public function questions()
    {
        return $this->hasMany('App\UserQuestion');
    }

    public function webinars()
    {
        return $this->belongsToMany('App\Webinar');
    }

    public function tracks()
    {
        return $this->belongsToMany('App\Track');
    }

    public function availableVouchers()
    {
        return $this->vouchers()->wherePivot('is_used', '=', Voucher::IS_AVAILABLE);
    }
}
