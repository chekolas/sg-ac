<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $casts = [
        'data' => 'array'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function uploads()
    {
        return $this->belongsToMany('App\Upload');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }
}
