<?php

namespace App\Listeners;

use App\Events\UserRegisteredEvent;
use App\Mail\UserVerificationMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class UserRegisteredListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    /**
     * Handle the event.
     *
     * @param UserRegisteredEvent $event
     * @return void
     */
    3.
    public function handle(UserRegisteredEvent $event)
    {
        $user = $event->user;

        $code = str_pad(rand(1, 999999),6, '0', STR_PAD_LEFT);

        $user->code = $code;
        $user->save();

        Mail::to($user)->send(new UserVerificationMail($user));
    }
}
