<?php

namespace App\Listeners;

use App\Events\WebinarStoredEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class WebinarStoredListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  WebinarStoredEvent  $event
     * @return void
     */
    public function handle(WebinarStoredEvent $event)
    {
        //
    }
}
