<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //
    public function tracks()
    {
        return $this->belongsToMany('App\Track')->withPivot(['page']);
    }

    public function contents()
    {
        return $this->belongsToMany('App\Content')->withPivot(['time']);
    }

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function users()
    {
        return $this->hasMany('App\UserQuestion');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function uploads()
    {
        return $this->belongsToMany('App\Upload');
    }
}
