<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Webinar extends Model
{
    use SoftDeletes;

    protected $casts = [
        'started_at' => 'datetime:Y-m-dTH:i:sP',
        'ended_at' => 'datetime:Y-m-dTH:i:sP'
    ];
    //
    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function uploads()
    {
        return $this->belongsToMany('App\Upload');
    }

    public function lecturers()
    {
        return $this->belongsToMany('App\Lecturer');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }
}
