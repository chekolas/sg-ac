<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'home'
], function () {
    Route::post('top-banners', 'HomeController@setTopBanners');
    Route::post('sub-banners', 'HomeController@setSubBanners');
    Route::post('body', 'HomeController@setBody');
    Route::post('header-menus', 'HomeController@setHeaderMenu');
    Route::post('footer-menus', 'HomeController@setFooterMenu');
});

Route::group([
    'prefix' => 'webinars'
], function () {
    Route::get('index', 'WebinarController@index');
    Route::post('store', 'WebinarController@store');
});

Route::group([
    'prefix' => 'tracks'
], function () {
    Route::get('index', 'TrackController@index');
    Route::post('store', 'TrackController@store');
});

Route::group([
    'prefix' => 'contents'
], function () {
    Route::get('index', 'ContentController@index');
    Route::post('store', 'ContentController@store');
});

Route::group([
    'prefix' => 'products'
], function () {
    Route::get('index', 'ProductController@index');
    Route::post('store', 'ProductController@store');
});

Route::group([
    'prefix' => 'vouchers'
], function () {
    Route::get('index', 'VoucherController@index');
    Route::post('store', 'VoucherController@store');
});

Route::group([
    'prefix' => 'users'
], function () {
    Route::get('index', 'UserController@index');
    Route::post('store', 'UserController@store');
});

Route::group([
    'prefix' => 'lecturers'
], function () {
    Route::get('index', 'LecturerController@index');
    Route::post('store', 'LecturerController@store');
});

Route::group([
    'prefix' => 'categories'
], function () {
    Route::get('index', 'CategoryController@index');
    Route::post('store', 'CategoryController@store');
});

Route::group([
    'prefix' => 'pages'
], function () {
    Route::get('index', 'PageController@index');
    Route::post('store', 'PageController@store');
});

Route::group([
    'prefix' => 'questions'
], function () {
    Route::get('index', 'QuestionController@index');
    Route::post('store', 'QuestionController@store');
    Route::post('answer', 'QuestionController@answer');
});
