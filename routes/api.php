<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
## Auth EndPoint ##
Route::post('login', 'AuthController@login');
Route::post('register', 'AuthController@register');
Route::get('verify', 'AuthController@verify')->middleware('auth:api');
Route::get('profile', 'AuthController@me')->middleware('auth:api');
Route::get('refresh', 'AuthController@refresh')->middleware('auth:api');
## Upload EndPoint ##
Route::post('upload', 'UploadController@store')->middleware('auth:api');
## Home EndPoint ##
Route::group([
    'prefix' => 'home'
], function () {
    Route::get('top-banners', 'HomeController@getTopBanners');
    Route::get('sub-banners', 'HomeController@getSubBanners');
    Route::get('header-menus', 'HomeController@getHeaderMenus');
    Route::get('footer-menus', 'HomeController@getFooterMenus');
    Route::get('body', 'HomeController@getBody');
});
## Product EndPoint ##
Route::group([
    'prefix' => 'products'
], function () {
    Route::get('index', 'ProductController@index');
    Route::post('index', 'ProductController@store')->middleware('auth:api');
});
## Invoice EndPoint ##
Route::group([
    'prefix' => 'invoices',
    'middleware' => 'auth:api'
], function () {
    Route::get('index', 'InvoiceController@index');
    Route::post('index', 'InvoiceController@store');
});

Route::group([
    'prefix' => 'vouchers',
], function () {
    Route::get('index', 'VoucherController@index');
});

Route::get('index', 'UploadController@index')->middleware('auth:api');
Route::post('index', 'UploadController@store')->middleware('auth:api');

Route::get('{slug}', 'PageController@index');
